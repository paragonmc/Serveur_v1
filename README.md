**Aucun code ici. Ce projet est réserver au signalement de bugs et problèmes.**
============================================

Retrouvez les codes de chaque projet dans ces derniers. **Pour signaler un problème se trouvant dans un code, merci de vous rendre dans le projet adapté et d'ouvrir une 'Issue' là bas ou de faire une 'Pull Request'.**


# Comment signaler un nouveau bug se trouvant sur le serveur ?


> **Avant de signaler le moindre problème, assurer vous que le problème est toujours d'actualité (de pouvoir le reproduire).**

Ensuite, rendez vous ici _(https://gitlab.com/paragonmc/Serveur_v1/issues)_ et cliquer sur "New issue" _(https://gitlab.com/paragonmc/Serveur_v1/issues/new)_. Vous arrivez sur une nouvelle page. A présent, il faut, au minimum, mettre :
- un titre _(le plus expressif possible, il doit expliquer un maximum votre problème)_,
- une description de votre soucis : la plus détaillé possible, ajouter s'y des images si besoin, etc...

Sur cet page, vous avez également la possibilité de choisir :
- un assignement : vous pouvez assigner le problème à une personne précise. Si vous ne savez pas qui mettre, laissez cette case vide; la personne concerné se l'assignera toute seule.
- une date : mettez celle à laquelle vous avez découvert le bug/problème.
- "Milestone" : _merci de laissez cette case vide_.
- un label (= un tag) : Chercher ici le nom du serveur sur lequel ce bug/problème est présent. Vous pouvez choisir "Global" si il se trouve sur tous les serveurs / sur le réseau entier et "Site web" si le problème se rapporte au site internet.
- une importance (un poids) : sachant que 1 est le plus faible et 9 la plus haute. Si votre problème est majeur, et empêche, par exemple, des dizaines de joueurs de jouer sur le serveur, vous aller choisir 9. Au contraire, s'il s'agit de l'oubli d'un caractère dans un message, vous aller plutôt choisir 1 ou 2. Si vous ne savez pas quelle importance mettre, laissez cette case vide ("No weight").

Une fois que vous avez tout remplis, il ne vous reste plus qu'à cliquer sur le bouton "Submit issue". Le problème va donc s'afficher dans la liste et pourra être traité par les personnes concernés.

Voici un exemple d'issue créer spécialement pour la démonstration : https://gitlab.com/paragonmc/Serveur_v1/issues/1.